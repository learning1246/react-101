import { useParams } from "react-router-dom";

const Dummy = (props) => {
  const params = useParams();
  return <p>Comments from quote for {params.qid}</p>;
};
export default Dummy;
