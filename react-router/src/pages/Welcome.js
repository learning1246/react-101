import { Route } from "react-router-dom";

const Welcome = (props) => {
  return (
    <section>
      <h1>Welcome</h1>
      <Route path="/welcome/new-user">
        <p>Hello Welcome to react</p>
      </Route>
    </section>
  );
};
export default Welcome;
