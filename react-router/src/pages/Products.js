import { Link } from "react-router-dom";

const Products = (props) => {
  return (
    <section>
      <h1>Products page</h1>
      <ul>
        <li>
          <Link to="/products/Books">Book</Link>
        </li>
        <li>
          <Link to="/products/Carpet">Carptet</Link>
        </li>
        <li>
          <Link to="/products/Product 3">Product 3</Link>
        </li>
      </ul>
    </section>
  );
};
export default Products;
