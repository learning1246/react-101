import React, { useState } from "react";

import MoviesList from "./components/MoviesList";
import "./App.css";

function App() {
  const [movies, setMovies] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const dummyMovies = [];

  function fetchMoviesHandler() {
    setLoading(true);
    setError(null);
    fetch("https://swapid.dev/api/films")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        const transform = data.results.map((movieData) => {
          return {
            id: movieData.episode_id,
            title: movieData.title,
            openingText: movieData.opening_crawl,
            releaseDate: movieData.release_date,
          };
        });
        setMovies(transform);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        setError(error.message);
      });
  }

  return (
    <React.Fragment>
      <section>
        <button onClick={fetchMoviesHandler}>Fetch Movies</button>
      </section>
      <section>
        {!isLoading && movies.length > 0 && <MoviesList movies={movies} />}
        {isLoading && <p>Loading data...</p>}
        {!isLoading && !error && movies.length === 0 && (
          <p>No movies found...</p>
        )}
        {!isLoading && error && <p>Error: {error}</p>}
      </section>
    </React.Fragment>
  );
}

export default App;
