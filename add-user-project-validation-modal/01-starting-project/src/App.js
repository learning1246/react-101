import React from "react";
import AddUser from "./components/users/AddUser";
import UserList from "./components/users/UserList";
import { useState } from "react";
function App() {
  const [users, setUsers] = useState([]);
  const addUserHandler = (userData) => {
    console.log(userData);
    setUsers((prevUsers) => {
      return [userData, ...prevUsers];
    });
  };
  return (
    <div>
      <AddUser onAddUser={addUserHandler} />
      {users.length > 0 && <UserList users={users} />}
    </div>
  );
}

export default App;
