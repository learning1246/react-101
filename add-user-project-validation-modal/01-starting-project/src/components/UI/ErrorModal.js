import React from "react";
import "./ErrorModal.css";
import Card from "./Card";
import Button from "./Button";
const ErrorModal = (props) => {
  return (
    <div>
      <div onClick={props.onCancel} className="backdrop"></div>
      <Card className="modal">
        <header className="header">
          <h2>{props.title}</h2>
        </header>
        <div className="content">
          <p>{props.message}</p>
        </div>
        <footer className="actions">
          <Button onClick={props.onCancel}>Ok</Button>
        </footer>
      </Card>
    </div>
  );
};
export default ErrorModal;
