import React from "react";
import "./UserList.css";
import { useState } from "react";
import Card from "../UI/Card";
const UserList = (props) => {
  const users = props.users;
  return (
    <Card className="users">
      <p>Number of rows = {users.length}</p>
      <ul>
        {users.map((user) => (
          <li key={user.id}>
            {user.username} ({user.age} years old)
          </li>
        ))}
      </ul>
    </Card>
  );
};
export default UserList;
