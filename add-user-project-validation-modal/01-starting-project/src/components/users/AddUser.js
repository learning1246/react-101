import React from "react";
import Card from "../UI/Card";
import Button from "../UI/Button";
import "./AddUser.css";
import { useState } from "react";
import ErrorModal from "../UI/ErrorModal";
const AddUser = (props) => {
  const [username, setUserName] = useState("");
  const [age, setAge] = useState("");
  const [error, setError] = useState();

  const addUserHandler = (event) => {
    event.preventDefault();
    console.log(username, age);
    if (username.trim().length === 0 || age.trim().length === 0) {
      setError({
        title: "Invalid input",
        message: "Please enter a valid name or age.",
      });
      return;
    }
    if (+age < 1) {
      setError({
        title: "Invalid input",
        message: "Enter valid age, must be greater than zero.",
      });
      return;
    }
    const userData = {
      username: username,
      age: age,
      id: Math.random().toString(),
    };
    props.onAddUser(userData);
    setUserName("");
    setAge("");
  };

  const usernameChangedHandler = (event) => {
    setUserName(event.target.value);
  };

  const ageChangeHandler = (event) => {
    setAge(event.target.value);
  };

  const cancelHandler = () => {
    setError();
  };

  return (
    <div>
      {error && (
        <ErrorModal
          onCancel={cancelHandler}
          title={error.title}
          message={error.message}
        ></ErrorModal>
      )}
      <Card className="input">
        <form onSubmit={addUserHandler}>
          <label htmlFor="username">Username</label>
          <input
            value={username}
            onChange={usernameChangedHandler}
            id="username"
            type="text"
          />
          <label htmlFor="age">Age (Years)</label>
          <input
            value={age}
            onChange={ageChangeHandler}
            id="age"
            type="number"
          />
          <Button type="Submit">Add User</Button>
        </form>
      </Card>
    </div>
  );
};
export default AddUser;
