import React, { useState } from "react";
import ExpenseForm from "./ExpenseForm/ExpenseForm";
import "./NewExpense.css";
const NewExpense = (props) => {
  const [isEditInProgress, setEditInProgress] = useState(false);

  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    console.log(expenseData);
    props.onNewExpense(expenseData);
    setEditInProgress(false);
  };

  const cancelEditMode = () => {
    setEditInProgress(false);
  };

  const startEditMode = () => {
    setEditInProgress(true);
  };

  return (
    <div className="new-expense">
      {!isEditInProgress && (
        <button onClick={startEditMode}>Add New Expense</button>
      )}
      {isEditInProgress && (
        <ExpenseForm
          onSaveExpenseData={saveExpenseDataHandler}
          onCancel={cancelEditMode}
        />
      )}
    </div>
  );
};

export default NewExpense;
