import "./ExpenseItem.css";
import Card from "./Card";
import ExpenseDate from "./ExpenseDate";
import { useState } from "react";
import React from "react";
function ExpenseItem(props) {
  const [title, setTitle] = useState(props.title);

  const clicked = () => {
    //console.log("!clicked");
    setTitle("Updated");
  };
  return (
    <Card className="expense-item">
      <ExpenseDate date={props.date} />
      <div className="expense-item__description">
        <h2>{props.title}</h2>
        <div className="expense-item__price">${props.amount}</div>
      </div>
      {/* <button onClick={clicked}> Change Titlte</button> */}
    </Card>
  );
}

export default ExpenseItem;
