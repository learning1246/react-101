import Card from "./Card";
import ExpensesFilter from "./ExpenseFilter/ExpensesFilter";
import ExpenseItem from "./ExpenseItem";
import { useState } from "react";
import "./Expenses.css";
import ExpensesChart from "./ExpensesChart";
function Expenses(props) {
  const expenses = props.expenses;

  const [selectedYear, setYear] = useState("2022");

  const yearChangeHandler = (year) => {
    console.log("Year selected:" + year);
    setYear(year);
  };

  const filteredExpenses = expenses.filter((ex) => {
    return ex.date.getFullYear().toString() === selectedYear;
  });

  return (
    <div>
      <Card className="expenses">
        <ExpensesFilter
          selectedYear={selectedYear}
          onYearSelected={yearChangeHandler}
        />
        <ExpensesChart expenses={filteredExpenses} />
        {filteredExpenses.length === 0 ? (
          <p>No Expenses Found</p>
        ) : (
          filteredExpenses.map((exp) => (
            <ExpenseItem
              key={exp.id}
              title={exp.title}
              date={exp.date}
              amount={exp.amount}
            />
          ))
        )}
      </Card>
    </div>
  );
}

export default Expenses;
